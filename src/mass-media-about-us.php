<?php require 'header.php'?>
<div class="container">
	<div class="mass-media-about-us">
		<div class="link">
			<ul>
				<li><a href="#">Главная</a></li>
				<li><img src="images/right-arrow.png" alt=""></li>
				<li><a href="#">СМИ о нас</a></li>
			</ul>
		</div>
		<div class="title">
			<h1>СМИ о нас</h1>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/smi-1.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">08.08.2019 для inAlmaty.kz</small></p>
						<h5 class="card-title">Интервью Азамата Жаманчинова руководителя проекта «Өнеге»</h5>
						<button type="button">Перейти</button>
					</div>
				</div>
			</div>
		</div>

		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/smi-2.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">02.08.2019	 radioTENGRI FM</small></p>
						<h5 class="card-title">Для слушателей «Наше все» Интервью Азамата Жаманчинова руководителя проекта «Өнеге»</h5>
						<button type="button">Перейти</button>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/smi-3.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">29.07.2019 для КТК</small></p>
						<h5 class="card-title">Интервью Азамата Жаманчинова руководителя проекта «Өнеге» и Светланы Богатыревой
магистра психологии</h5>
						<button type="button">Перейти</button>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/smi-4.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">12.07.2019 Для Нового телевидения</small></p>
						<h5 class="card-title">Интервью Азамата Жаманчинова руководителя проекта «Өнеге»</h5>
						<button type="button">Перейти</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>












<?php require 'footer.php'?>