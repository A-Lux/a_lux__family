<?php require 'header.php'?>


<div class="container">
	<div class="col-xl-2 p-0">
		<div class="link">
			<ul>
				<li><a href="#">Главная</a></li>
				<li><img src="images/right-arrow.png" alt=""></li>
				<li><a href="#">Библиотека</a></li>
			</ul>
		</div>
	</div>
	<div class="library">
		<h1>Библиотека</h1>
		<br>
		<div class="tabs">
			<p class="text-muted">Выберите раздел</p>
			<ul class="p-0 d-flex">
				<li class="tab-activ">Для мам</li>
				<li>Для пап</li>
				<li>Для друзей</li>
				<li>Для общего ознакомления</li>
			</ul>
		</div>
	</div>

	<div class="library-content">
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-1.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Качественное времяпровождение с семьей и работа</h5>
						<p class="card-text">Качественное проведение времени с ребенком помогает устанавливать хорошие взаимоотношения</p>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-2.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Мы ничем не жертвуем ради ребёнка</h5>
						<p class="card-text">Как правило, когда в семье появляется ребёнок, молодые родители кардинально меняют свой образ жизни. Они посвящают всё время малышу и ведению домашнего хозяйства, а также жертвуют привычными увлечениями и интересами, причём особенно это справедливо для мам.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-3.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Я не пользовалась телефоном, пока была рядом с ребёнком</h5>
						<p class="card-text">В начале мая на вечернем шоу Конана О’Брайена главный американский стендапер Луи Си Кей рассказал о необычном педагогическом приёме. Комик, удручённый тем, что даже наедине со своими детьми он не может оторваться от телефона, попросил дочь установить защитный код, ограничивающий ему доступ в интернет. Благодаря этому он смог больше времени уделять своим детям и быть внимательнее рядом с ними.

						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-4.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Дочь три дня выбирала мне одежду</h5>
						<p class="card-text">Четырёхлетний ребёнок редактора The Village решал, в чём мама пойдёт на работу</p>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-5.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Одна на море с детьми: 10 правил пляжного отдыха</h5>
						<p class="card-text">Мама двоих детей советует, как наладить быт на курорте — от выбора места до полезных мелочей</p>
					</div>
				</div>
			</div>
		</div>
		<div class="library-card">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="images/library-6.png" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<p class="card-text"><small class="text-muted">23 августа 2019</small></p>
						<h5 class="card-title">Одинокая мама — о том, как жить без денег и вести позитивный блог в Instagram</h5>
						<p class="card-text">Качественное проведение времени с ребенком помогает устанавливать хорошие взаимоотношения</p>
					</div>
				</div>
			</div>
		</div>
		<div class="show-more text-center">
			<button type="button">Показать еще</button>
		</div>
	</div>
</div>	








<?php require 'footer.php'?>