	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Őnege | Семейная академия</title>
		<meta name="description" content="Реализация комплекса социальных проектов по оказанию семейных консультаций">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
		<link rel="stylesheet" href="/a_lux__family/dist/css/owl.carousel.min.css">
		<link rel="stylesheet" href="/a_lux__family/dist/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="/a_lux__family/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/a_lux__family/dist/css/main.css">
	</head>
	<body>
