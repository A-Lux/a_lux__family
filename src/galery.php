<?php require 'header.php'?>

<div class="container">
	<div class="galery">
		<div class="link">
			<ul>
				<li><a href="#">Главная</a></li>
				<li><img src="images/right-arrow.png" alt=""></li>
				<li><a href="#">Галерея</a></li>
			</ul>
		</div>
		<div class="family-title">
			<h1>Галерея</h1>
		</div>
		<div class="galery-tabs">
			<br>
			<p class="text-muted">Выберите город</p>

			<ul class="p-0 d-flex">
				<li class="tab-activ">Все города</li>
				<li>Уральск</li>
				<li>Актобе</li>
				<li>Актау</li>
				<li>Кызылорда</li>
				<li>Шымкент</li>
				<li>Туркестан</li>
				<li>Талгар</li>
				<li>Алматы</li>
				<li>Павлодар</li>
				<li>Петропавловск</li>
			</ul>
		</div>
		<br>
		<div class="galety-content">
			<div class="col-xl-5 p-0">
				<div class="galery-content-title">
				<ul>
					<li><b>23 мая 2019</b></li>
					<li><h4>Название темы</h4></li>
				</ul>
			</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xl-3">
					<img src="images/galery-1.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-2.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-3.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-4.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-5.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-6.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<button>Посмотреть все</button>
			</div>
		</div>
		<div class="galety-content">
			<div class="col-xl-5 p-0">
				<div class="galery-content-title">
				<ul>
					<li><b>23 мая 2019</b></li>
					<li><h4>Название темы</h4></li>
				</ul>
			</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xl-3">
					<img src="images/galery-1.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-2.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-3.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-4.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-5.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-6.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<button>Посмотреть все</button>
			</div>
		</div>
		<div class="galety-content">
			<div class="col-xl-5 p-0">
				<div class="galery-content-title">
				<ul>
					<li><b>23 мая 2019</b></li>
					<li><h4>Название темы</h4></li>
				</ul>
			</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xl-3">
					<img src="images/galery-1.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-2.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-3.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-4.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-5.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-6.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<div class="col-xl-3">
					<img src="images/galery-7.png" alt="">
				</div>
				<button>Посмотреть все</button>
			</div>
		</div>
	</div>
</div>
















<?php require 'footer.php'?>