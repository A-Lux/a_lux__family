/* —---------------------------------------------------— */
$('.slider-story').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        500:{
            items:1
        },
        600:{
            items:2
        },
        900:{
            items:2
        },
        1000:{
            items:3
        },
        2000:{
            items:3
        }
    }
});

var nav = $('.slider-story'); 
// Go to the next item 
$('.right').click(function() { 
nav.trigger('next.owl.carousel', [600]); 
}) 
// Go to the previous item 
$('.left').click(function() { 
nav.trigger('prev.owl.carousel', [600]); 
});

/* —---------------------------------------------------— */

/* Навигация по странице 
—---------------------------------------------------— */
$('.mobile-menu').click(function () {
    $('.mobile-window').addClass('nav-open');
    $('body').addClass('modal-open');
    return false;
});


$('.mobile-menu-close').click(function () {
    $('.mobile-window').removeClass('nav-open');   
    $('body').removeClass('modal-open');
    return false;
});


$(function () {
    $(document).click(function (event) {
        if ($('.mobile-window').hasClass('nav-open')) {
            if ($(event.target).closest('.mobile-window').length) {
                return;
            }
            $('.mobile-window').removeClass('nav-open');
            $('body').removeClass('modal-open');
            event.stopPropagation();
        }
    });
});


